import math
import struct
import time
import wave
import progressbar

class Sequencer:
    def __init__(self):
        self.reset()

    def reset(self):
        self.n = 0.0
        self.b = 0.0
        self.x = 0.0

    def D(self, t, u, v):
        self.b += float(u)
        if t > self.b:
            self.x = self.b
            self.n = float(v)


def clamp(x, a, b):
    if x < a:
        return a
    elif x > b:
        return b

    return x


def smoothstep(edge0, edge1, x):
    x = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0)
    return x * x * (3 - 2 * x)


def mainSound(time):
    time = math.fmod(time, 40.0)
    X = Sequencer()
    left = 0.0
    right = 0.0
    i = 0
    h = float(i) / (3.0 - 1.0)
    t = (time - 0.57 * h) / 0.18

    X.reset()
    X.D(t, 10, 71)
    X.D(t, 2, 76)
    X.D(t, 3, 79)
    X.D(t, 1, 78)
    X.D(t, 2, 76)
    X.D(t, 4, 83)
    X.D(t, 2, 81)
    X.D(t, 6, 78)
    X.D(t, 6, 76)
    X.D(t, 3, 79)
    X.D(t, 1, 78)
    X.D(t, 2, 74)
    X.D(t, 4, 77)
    X.D(t, 2, 71)
    X.D(t, 10, 71)
    X.D(t, 2, 76)
    X.D(t, 3, 79)
    X.D(t, 1, 78)
    X.D(t, 2, 76)
    X.D(t, 4, 83)
    X.D(t, 2, 86)
    X.D(t, 4, 85)
    X.D(t, 2, 84)
    X.D(t, 4, 80)
    X.D(t, 2, 84)
    X.D(t, 3, 83)
    X.D(t, 1, 82)
    X.D(t, 2, 71)
    X.D(t, 4, 79)
    X.D(t, 2, 76)
    X.D(t, 10, 79)
    X.D(t, 2, 83)
    X.D(t, 4, 79)
    X.D(t, 2, 83)
    X.D(t, 4, 79)
    X.D(t, 2, 84)
    X.D(t, 4, 83)
    X.D(t, 2, 82)
    X.D(t, 4, 78)
    X.D(t, 2, 79)
    X.D(t, 3, 83)
    X.D(t, 1, 82)
    X.D(t, 2, 70)
    X.D(t, 4, 71)
    X.D(t, 2, 83)
    X.D(t, 10, 79)
    X.D(t, 2, 83)
    X.D(t, 4, 79)
    X.D(t, 2, 83)
    X.D(t, 4, 79)
    X.D(t, 2, 86)
    X.D(t, 4, 85)
    X.D(t, 2, 84)
    X.D(t, 4, 80)
    X.D(t, 2, 84)
    X.D(t, 3, 83)
    X.D(t, 1, 82)
    X.D(t, 2, 71)
    X.D(t, 4, 79)
    X.D(t, 2, 76)

    note_freq = 440.0 * math.pow(2.0, (X.n - 69.0) / 12.0)
    note_time = 0.18 * (t - X.x)

    y = (
        0.5
        * math.sin(6.2831 * 1.00 * note_freq * note_time)
        * math.exp(-0.0015 * 1.0 * note_freq * note_time)
    )
    y += (
        0.3
        * math.sin(6.2831 * 2.01 * note_freq * note_time)
        * math.exp(-0.0015 * 2.0 * note_freq * note_time)
    )
    y += (
        0.2
        * math.sin(6.2831 * 4.01 * note_freq * note_time)
        * math.exp(-0.0015 * 4.0 * note_freq * note_time)
    )
    y += 0.1 * y * y * y
    y *= 0.9 + 0.1 * math.cos(40.0 * note_time)
    y *= smoothstep(0.0, 0.01, note_time)

    left += y * (0.5 + 0.2 * h) * (1.0 - math.sqrt(h) * 0.85)
    right += y * (0.5 - 0.2 * h) * (1.0 - math.sqrt(h) * 0.85)

    return (left, right)


def save_wav(file_name, sample_rate, audio):
    wav_file = wave.open(file_name, "w")
    sampwidth = 2
    nframes = len(audio)
    comptype = "NONE"
    compname = "not compressed"
    wav_file.setparams((2, sampwidth, sample_rate, nframes, comptype, compname))

    for l, r in audio:
        wav_file.writeframes(struct.pack("h", int(l * 32767.0)))
        wav_file.writeframes(struct.pack("h", int(r * 32767.0)))

    wav_file.close()


if __name__ == "__main__":
    SAMPLE_RATE = 44100
    SONG_LENGTH = 40
    SAMPLES = SONG_LENGTH * SAMPLE_RATE
    filename = "output.wav"
    start = time.time()
    audio = []

    with progressbar.ProgressBar(max_value=SAMPLES) as bar:
        for sample in range(SAMPLES):
            t = float(sample) / float(SAMPLE_RATE)
            audio.append(mainSound(t))
            bar.update(sample)

    print(f"Writing wav file {filename}...")
    save_wav(filename, SAMPLE_RATE, audio)
    print("Elapsed time:", time.time() - start)
