#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
runs the sequencer on the example score (hedwig's theme from harry potter)
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2021, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "bogdan@iddo.ro"
__status__ = "Development"  # can also be "Prototype" or "Production"


def main():
    import time
    from audio_sequencer.songbook import harry_potter_theme
    from audio_sequencer.disk_access import WavFileWriter
    from audio_sequencer.sequencer import sequence

    sample_rate = 44100
    sample_seconds = 40
    out_fpath = '../output.wav'

    samples, repetitions, suffix_samples = sequence(harry_potter_theme, sample_rate, sample_seconds=sample_seconds,
                                                    legacy_bug_compatibility=True, print_time=True)

    sampling_time = time.time()

    assert repetitions == 1
    assert suffix_samples is None
    with WavFileWriter(out_fpath, sample_rate * sample_seconds, sample_rate) as wav_fh:
        wav_fh.write(samples)

    print("fwrite time:", time.time() - sampling_time)


if __name__ == '__main__':
    import os
    import sys
    if __name__ == '__main__':
        try:
            main()
        except KeyboardInterrupt:
            # try to do some cleaning up before exiting (i.e., wait for disk write to finish, and close wav file)
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)