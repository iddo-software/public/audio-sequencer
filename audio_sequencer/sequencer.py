#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
audio sequencer
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2021, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "bogdan@iddo.ro"
__status__ = "Development"  # can also be "Prototype" or "Production"

import math

import numpy

from audio_sequencer.voices import voice1


def _get_song_params_from_score(score):
    """
    helper function for the `Song` class, converting the data from the format in which it is stored by instances of the
    `songbook.Score` class to the format used by the `Song` class
    :param score: an instance of the `songbook.Score` class
    :return: arguments for `Song.__init__`: note_codes, note_stops_eighths, last_note_seconds, seconds_per_eighth_note
    """

    # param checks and basic preproc
    notes_rel_offset_eighths = score.notes_offset_and_code[:, 0]
    notes_code = score.notes_offset_and_code[:, 1]

    # store note durations as int64s if no truncation occurs
    notes_rel_offset_eighths_int64 = notes_rel_offset_eighths.astype(numpy.int64)
    if numpy.all(notes_rel_offset_eighths_int64 == notes_rel_offset_eighths):
        notes_rel_offset_eighths = notes_rel_offset_eighths_int64

    # store notes as int8s if no truncation occurs
    notes_code_int8 = notes_code.astype(numpy.int8)
    if numpy.all(notes_code_int8 == notes_code):
        notes_code = notes_code_int8

    # compute all note durations, except for the last note (and prepend a null note at the beginning, if necessary)
    null_note_duration_eighths = notes_rel_offset_eighths[0]
    if null_note_duration_eighths:
        notes_code = numpy.insert(notes_code, 0, 0)
        notes_duration_eighths = notes_rel_offset_eighths
    else:
        notes_duration_eighths = notes_rel_offset_eighths[1:]

    # remove notes with 0 duration
    if numpy.issubdtype(notes_duration_eighths.dtype, numpy.integer):
        invalid_note_mask = notes_duration_eighths == 0
    else:
        assert numpy.issubdtype(notes_duration_eighths.dtype, numpy.floating)
        invalid_note_mask = numpy.isclose(notes_duration_eighths, 0)

    if numpy.any(invalid_note_mask):
        valid_note_mask = ~invalid_note_mask
        notes_code = notes_code[valid_note_mask]
        # notes_rel_offset_eighths = notes_rel_offset_eighths[valid_note_mask]
        notes_duration_eighths = notes_duration_eighths[valid_note_mask]

    # we only do cumsum here, on int64, so we avoid accumulating floating point precision errors
    notes_stop_eighths = numpy.cumsum(notes_duration_eighths)

    return notes_code, notes_stop_eighths, score.duration_seconds, score.seconds_per_eighth_note


null_note_freq = 440 * numpy.exp2(-69 / 12)


def _get_note_freqs(note_codes):
    """
    compute note frequencies in an efficient fashion (both in terms of memory and execution speed)
    :param note_codes:
    :return:
    """
    # there are notes that are used multiple times, and we need to compute their frequencies only once
    unique_notes, note_indices = numpy.unique(note_codes, return_inverse=True)
    # 440Hz is the frequency of A4; 69 is A4's code; 12 is the number of semitones in an octave; every octave doubles
    # the frequency
    # TODO: use numexpr (not critical, since there are, usually, just a few notes, compared to the number of samples)
    unique_note_freqs = 440 * numpy.exp2((unique_notes - 69) / 12)
    note_freqs = unique_note_freqs[note_indices]

    return note_freqs


class Song(object):
    def __init__(self, note_freqs, note_stops, duration_seconds, note_time_offset_seconds, first_note_time_seconds,
                 period=None, sample_rate=None, legacy_bug_null_first_sample=False, legacy_bug_offset=False):
        """
        Convert song scores to a format that helps avoid numerical precision issues
        :param note_freqs: float array - contains the frequencies of the notes in the song; unlike songbook.Score,
        we explicitly store the pause (note with code 0) in the beginning of the song
        :param note_stops: int64 array - contains the stop index (in note eighths) of every note (except for the
        last one, which is defined by the difference between duration_seconds and note_stops[-1])
        :param duration_seconds: float - the duration of song, in seconds (see doc for note_stops_eighths)
        :param note_time_offset_seconds: float representing where to start sampling in the (first eighth of the) first
        note (expressed in seconds)
        - if there's more than one note in the song, it should be in the interval [0, sample_period)
        - if there's exactly one note in the song, it should be 0
        :param: first_note_time_seconds: float - used when cropping a song in the middle of a note (note: this is
        relative to the note's start)
        :param period: float - the duration of an eighth note, in seconds (note: specify exactly one of `period` and
        `sample_rate`)
        :param sample_rate: int - number of samples per second (note: specify exactly one of `period` and `sample_rate`)
        """
        # param checks

        assert len(note_stops) == len(note_freqs) - 1

        # exactly one of period and sample_rate must be specified
        assert bool(period) != bool(sample_rate)
        if period:
            self.period = period
        if sample_rate:
            self.sample_rate = sample_rate

        self.note_freqs = note_freqs
        self.note_stops = note_stops
        self.duration_seconds = duration_seconds
        self.note_time_offset_seconds = note_time_offset_seconds
        self.first_note_time_offset_seconds = first_note_time_seconds
        self.legacy_bug_null_first_sample = legacy_bug_null_first_sample
        self.legacy_bug_offset = legacy_bug_offset
        self.original = None

    @property
    def period(self):
        if self._period:
            return self._period
        else:
            return 1 / self.sample_rate

    @period.setter
    def period(self, value):
        self._period = value
        self._sample_rate = None

    @property
    def sample_rate(self):
        if self._sample_rate:
            return self._sample_rate
        else:
            return 1 / self.period

    @sample_rate.setter
    def sample_rate(self, value):
        self._sample_rate = value
        self._period = None

    @property
    def note_time_offset_seconds(self):
        return self._note_time_offset_seconds

    @note_time_offset_seconds.setter
    def note_time_offset_seconds(self, value):
        assert value >= 0
        if len(self.note_freqs) == 1:
            assert not value
        else:
            assert value < self.period
        self._note_time_offset_seconds = value

    @staticmethod
    def from_score(score, legacy_bug_compatibility=False):
        """
        factory method
        :param score: a songbook.Score instance
        :return: the corresponding Song instance
        """
        note_codes, note_stops_eighths, duration_seconds, seconds_per_eighth_note = _get_song_params_from_score(score)
        note_freqs = _get_note_freqs(note_codes)
        song = Song(note_freqs, note_stops_eighths, duration_seconds,
                    note_time_offset_seconds=0, first_note_time_seconds=0, period=seconds_per_eighth_note,
                    legacy_bug_null_first_sample=legacy_bug_compatibility, legacy_bug_offset=legacy_bug_compatibility)
        return song

    def crop(self, crop_start_seconds=None, crop_stop_seconds=None,
             crop_start_sample_idx=None, crop_stop_sample_idx=None):
        """
        Removes notes from the song that are outside of the range defined by `start` and `stop`. Both `start` and `stop`
        represent time offsets relative to the start of the song. Any of `start` and `stop` can be expressed either in
        units of seconds or in units equal to sampling period. However, none of them are allowed to be specified in
        both.
        :param crop_start_seconds: float - crop range start (in seconds)
        :param crop_stop_seconds: float - crop range stop (in seconds)
        :param crop_start_sample_idx: int - crop range start (in samples)
        :param crop_stop_sample_idx: int - crop range stop
        :return: a new Song object containing only notes that would be sampled withing the specified range
        """

        # make sure caller does not double define params
        assert not (bool(crop_start_sample_idx) and bool(crop_start_seconds))
        assert not (bool(crop_stop_sample_idx) and bool(crop_stop_seconds))

        # check and preproc params
        def get_boundary(boundary_seconds, boundary_sample_idx):
            if boundary_seconds:
                # convert boundary to periods
                boundary = boundary_seconds / self.period

                # some integers can't be represented in floating point precision, so we snap to the closest int if the
                # difference is insignificant TODO: disable if messes things up (shouldn't, though)
                closest_int = round(boundary)
                if math.isclose(boundary, closest_int):
                    boundary = int(closest_int)
            elif boundary_sample_idx:
                boundary = boundary_sample_idx
                boundary_seconds = boundary * self.period
            else:
                boundary = None

            if boundary_seconds:  # is not None
                # do comparison in seconds because it's probably more numerically stable (mul vs. div)
                assert 0 <= boundary_seconds <= self.duration_seconds

            return boundary, boundary_seconds

        crop_start, crop_start_seconds = get_boundary(crop_start_seconds, crop_start_sample_idx)

        if crop_stop_seconds == self.duration_seconds:
            # quit early if nothing to do
            crop_stop_seconds = None

        crop_stop, crop_stop_seconds = get_boundary(crop_stop_seconds, crop_stop_sample_idx)

        if crop_start_seconds and crop_stop_seconds:  # are not None
            # TODO: handle case when crop_start_seconds == crop_stop_seconds (maybe return None?)
            assert crop_start_seconds <= crop_stop_seconds

        # ~check and preproc params

        # helper variable (used for some sanity checks)
        last_note_seconds = self.duration_seconds - self.note_stops[-1] * self.period

        # this is passed to the init of the cropped song
        cropped_duration_seconds = self.duration_seconds
        if crop_start_seconds and crop_stop_seconds:  # are not None
            cropped_duration_seconds = crop_stop_seconds - crop_start_seconds
        elif crop_start_seconds:
            cropped_duration_seconds -= crop_start_seconds
        elif crop_stop_seconds:
            cropped_duration_seconds -= crop_stop_seconds

        # this is subtracted from all note_stops of the cropped song (shortens the first note)
        crop_start_n_skipped_periods = 0
        # this is passed to the init of the cropped song
        cropped_note_time_offset_seconds = self.note_time_offset_seconds
        if crop_start_seconds:
            # adjust the cropped song's note_time_offset_seconds
            crop_start_n_skipped_periods = int(math.floor(crop_start))
            if int(crop_start) != crop_start:
                # if crop_start is an int, we crop right on a sample border, so we don't need to adjust the offset
                cropped_note_time_offset_seconds -= crop_start_seconds - crop_start_n_skipped_periods * self.period

            # sanity check
            assert 0 <= cropped_note_time_offset_seconds <= self.period

            # search for the first note that ends after crop_start
            # - we use side='right', because, if it happens that we're looking for a position which happens to be a note
            # stop, then the next note is the first one we care about, since the previous one just ended
            crop_start_note_idx = numpy.searchsorted(self.note_stops, crop_start, side='right')

            if crop_start_note_idx == len(self.note_stops):
                # if all the notes end before the sample start, then it must mean that we're just sampling the last note
                # (remember, the note_stops array does NOT provide the stop of the last note, the last note's
                # duration being stored separately)

                # sanity check: if we don't have an extra note in the original song, we can't sample from it
                if math.isclose(last_note_seconds, 0):
                    assert not cropped_duration_seconds

                cropped_first_note_time_offset_seconds = self.duration_seconds - cropped_duration_seconds
            else:
                # the sampling starts in the middle of a note; we're going to want to crop away (i.e., subtract from the
                # note's duration or, alternatively, from the stop of the note and of all the subsequent notes) the
                # number of periods of that note that occur before the sampling start moment;

                # compute first note's note_time by adding the time of the cropped periods
                crop_start_note_start = self.note_stops[crop_start_note_idx - 1] if crop_start_note_idx else 0

                # the number of periods cropped from the first note
                n_crop_start_note_skipped_periods = crop_start_n_skipped_periods - crop_start_note_start
                # convert to seconds (before, this used to include cropped_note_time_offset_seconds here)
                cropped_first_note_time_offset_seconds = n_crop_start_note_skipped_periods * self.period

                # if cropping starts at first note, also take into account current offset
                if not crop_start_note_idx:
                    cropped_first_note_time_offset_seconds += self.first_note_time_offset_seconds
        else:
            # this is used to crop the notes and stops of this song
            crop_start_note_idx = 0
            # this is passed to the init of the cropped song
            cropped_first_note_time_offset_seconds = 0

        # this number is subtracted from note_stops[-1] of the cropped song
        cropped_last_note_n_skipped_periods = 0
        # if this is true, the duration of the last note of the cropped song will be represented explicitly instead of
        # using an int number of periods
        cropped_drop_last_note_stop = False
        if crop_stop_seconds:
            # helper array containing note start offsets (in periods) - not exactly necessary, since we could do
            # with self.note_stops, but it's not a great performance hit (unless we have huge songs) and it
            # makes the reader's life easier
            note_starts = numpy.insert(self.note_stops, 0, 0)

            # find the first note that starts after crop_stop:
            # - start searching at start_idx
            # - use side='left' so we crop away the note that starts exactly at the same time as we stop sampling
            crop_stop_note_idx = crop_start_note_idx + numpy.searchsorted(note_starts, crop_stop,
                                                                          side='left')

            if crop_stop_note_idx == len(note_starts):
                # if all the notes start before crop_stop, then it must mean that the end of the sample is
                # somewhere within the last note

                # sanity check: if we don't have an extra note in the original song, we can't sample from it
                if not last_note_seconds:
                    assert self.duration_seconds == crop_stop_seconds
            else:
                if isinstance(crop_stop, int):
                    # the sampling ends exactly on the end of a period, so we don't need to keep the duration of
                    # the last note in seconds, since we can represent it in an integer number of periods, which is
                    # preferable
                    # cropped_last_note_seconds = 0

                    # the last note stop will end up being equal to crop_stop
                    cropped_last_note_n_skipped_periods = self.note_stops[crop_stop] - crop_stop
                else:
                    # we represent the last note's duration, which is not a multiple of a period, in seconds
                    cropped_drop_last_note_stop = True

                    # crop_stop_note_idx should never be 0, since note_starts[0] == 0 and the only way in which
                    # crop_stop_note_idx would end up to be 0 is if crop_stop == 0, which is possible only if
                    # crop_stop_seconds == 0 (or very, very close to it). If crop_stop_seconds == 0, we shouldn't get
                    # here, anyway. This assertion breaks only if someone requests an absurdly low sampling offset,
                    # case in which we'd like to be notified.
                    assert crop_stop_note_idx

            # sanity check
            assert crop_stop_note_idx >= crop_start_note_idx
        else:
            # this is used to crop the notes and stops of this song
            crop_stop_note_idx = None

        if not any((crop_start_note_idx, crop_stop_note_idx, cropped_duration_seconds,
                    cropped_note_time_offset_seconds, cropped_first_note_time_offset_seconds,
                    cropped_drop_last_note_stop, cropped_last_note_n_skipped_periods, crop_start_n_skipped_periods)):
            # if no cropping is required just return a copy of this song
            song = Song(self.note_freqs, self.note_stops, self.duration_seconds,
                        self.note_time_offset_seconds, self.first_note_time_offset_seconds,
                        self.period, legacy_bug_null_first_sample=self.legacy_bug_null_first_sample,
                        legacy_bug_offset=self.legacy_bug_offset)
            return song

        if not crop_stop_note_idx:
            crop_stop_note_idx = len(self.note_freqs)

        # make copies so that, if caller modifies return value, self remains the same
        cropped_note_freqs = self.note_freqs[crop_start_note_idx: crop_stop_note_idx].copy()
        cropped_note_stops = self.note_stops[crop_start_note_idx:
                                             crop_stop_note_idx - cropped_drop_last_note_stop].copy()

        # the order of the following operations should remain the same
        if cropped_last_note_n_skipped_periods:
            cropped_note_stops[-1] -= cropped_last_note_n_skipped_periods

        if crop_start_n_skipped_periods:
            cropped_note_stops = cropped_note_stops - crop_start_n_skipped_periods

        # we don't want to make the first sample a '0' note however, we still want to adjust stops
        if crop_start_note_idx or crop_start_n_skipped_periods:
            legacy_bug_null_first_sample = False
        else:
            legacy_bug_null_first_sample = self.legacy_bug_null_first_sample

        cropped = Song(cropped_note_freqs, cropped_note_stops, cropped_duration_seconds,
                       cropped_note_time_offset_seconds, cropped_first_note_time_offset_seconds,
                       self.period, legacy_bug_null_first_sample=legacy_bug_null_first_sample,
                       legacy_bug_offset=self.legacy_bug_offset)
        cropped.original = self

        return cropped

    def resample(self, dst_sample_rate):
        """
        change sampling rate (also changes note_stops type from int64 to float !!!)
        :param dst_sample_rate: the desired new sample rate
        :return: a new Song object with the specified sample rate
        """

        # TODO: also test downsampling (not just upsampling)

        # [src periods] -> [seconds]:
        # note_stops [seconds] = self.note_stops [src periods] * self.period [seconds / src period]
        # [seconds] -> [dst periods] note_stops to dst_sample_rate
        # note_stops [dst periods] = note_stops [seconds] * dst_sample_rate [dst period / second]
        note_stops = (self.period * dst_sample_rate) * self.note_stops

        # TODO: this check might not be necessary; try and remove
        if self.legacy_bug_offset:
            # don't try to fix numerical floating point precision errors, since we might need them to reproduce the bugs
            # in the legacy implementation
            pass
        else:
            # try to cast to int64 if no truncation occurs (i.e., if dst_sample_rate is evenly divisible by self.period)
            # actually, I think a better explanation would be that a single source sample can be exactly replaced by and
            # integer number of dst samples;
            # TODO: try using float.as_integer_ratio to make this process more precise
            note_stops_int64 = numpy.round(note_stops).astype(numpy.int64)
            int_mask = numpy.isclose(note_stops, note_stops_int64)
            n_close = numpy.count_nonzero(int_mask)
            if n_close == len(int_mask):  # numpy.all(int_mask)
                note_stops = note_stops_int64
            elif n_close:  # numpy.any(int_mask)
                note_stops[int_mask] = note_stops_int64[int_mask]

        first_note_time_offset_seconds = self.first_note_time_offset_seconds
        note_time_offset_seconds = self.note_time_offset_seconds
        # compute the number of samples that fit into note_time_offset_seconds
        # n_samples_to_subtract = int(self.note_time_offset_seconds / dst_period)
        n_samples_to_subtract = int(self.note_time_offset_seconds * dst_sample_rate)
        if n_samples_to_subtract:
            seconds_to_subtract = n_samples_to_subtract / dst_sample_rate
            note_time_offset_seconds -= seconds_to_subtract  # < dst_period
            first_note_time_offset_seconds += seconds_to_subtract
            note_stops[0] -= n_samples_to_subtract

        song = Song(self.note_freqs, note_stops, self.duration_seconds,
                    note_time_offset_seconds, first_note_time_offset_seconds, sample_rate=dst_sample_rate,
                    legacy_bug_null_first_sample=self.legacy_bug_null_first_sample,
                    legacy_bug_offset=self.legacy_bug_offset)
        # we store self as the original source of the resampled song (we use it to reproduce rounding errors in the
        # original implementation)
        song.original = self

        return song

    def sequence(self, voice):
        n_samples = int(self.duration_seconds * self.sample_rate)  # implicitly applies floor

        # when calling resample, self.note_stops might have been cast to float, so we int it here, too
        note_stops_int = self.note_stops.astype(numpy.int64)  # implicitly applies floor

        if self.legacy_bug_offset:
            # The problem:
            # ~~~~~~~~~~~~
            # In the legacy implementation, if it that the sampling point is exactly at the beginning of a note, it
            # will, actually, sample the previous note, instead. This happens because the condition used to identify the
            # note which contains the current sample reads `if t > self.b:` instead of `if t >= self.b:` at line 18 of
            # the file `legacy.py`. This means that the first sample of some notes is offset by `1`, and that the note
            # code of the first sample of every song must be `0`, even if, in the score, the relative offset of the
            # first note (which encodes the length of the pause from playback start to the first note) is `0`.
            #
            # Points of concern:
            # ~~~~~~~~~~~~~~~~~~
            # Since we've done our best to avoid any numerical floating point precision errors, we now need to take into
            # account the numerical precision errors that are introduced by the operations of the original legacy
            # implementation. Here's a list of the sources of errors in the legacy version:
            # - sample_offset_seconds (float): obtained by dividing the (integer) sample index by the
            # (integer) sample rate (both are converted to floats beforehand)
            # - sample_offset_seconds = fmod(sample_offset_seconds, song.duration_seconds)
            # - sample_offset_eighths (float - in units of time equal to the duration of an eighth = 0.18 in harry
            # potter): is obtained by dividing sample_offset_seconds by eighth_duration
            # - note absolute offsets are accumulated from relative note offsets (in eighths), which have been
            # converted to floats - bound to accumlate errors, too
            #
            # How we're addressing this:
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~
            # We need to reproduce all the steps presented in Points of concern. After several failed attempts, we
            # concluded that we also need to reproduce the errors in the sampling points, not just in the note ends.
            # (I.e., we tried using integer sampling points in the final sampling rate, this fixed some issues, but
            # not all of them)

            # debug code
            # ref_data = numpy.load('../reference_data.npz')
            # ref_note_freqs = ref_data['note_freqs']
            # ref_note_times = ref_data['note_times']
            # ref_note_stop_mask = ref_note_freqs[1:] != ref_note_freqs[:-1]
            # ref_note_stops = numpy.nonzero(ref_note_stop_mask)[0] + 1
            # # original song has two consecutive 71 notes at indices 13 and 14, so we can't deduce their separation index
            # # (we insert a 0 instead)
            # ref_note_stops = numpy.insert(ref_note_stops, 14, 0)

            # rebuild the stops array by accumulating floats, in eighths (we don't use cumsum because it might have a
            # different execution order and might, thus, introduce different float errors)
            note_stops_eighths = [float(self.original.note_stops[0])]  # todo: check self.original.stops.dtype == int
            for note1_stop, note2_stop in zip(self.original.note_stops[:-1], self.original.note_stops[1:]):
                note_stops_eighths.append(note_stops_eighths[-1] + float(note2_stop - note1_stop))
            note_stops_eighths = numpy.asarray(note_stops_eighths)

            def sample_idx2eighths(_sample_indices):
                # group operations to enforce the same order as in the legacy code
                sample_seconds = _sample_indices.astype(float) / float(self.sample_rate)
                sample_seconds2 = numpy.fmod(sample_seconds, float(self.duration_seconds))
                sample_eighths = sample_seconds2 / float(self.original.period)
                return sample_eighths

            # compute the time offset of the samples found at the indices between notes
            note_stop_sample_offset_eighths = sample_idx2eighths(note_stops_int)

            # ~(note_stop_sample_offset_eighths > note_stops_eighths)
            inc_mask = note_stop_sample_offset_eighths <= note_stops_eighths
            if numpy.any(inc_mask):
                note_stops_int[inc_mask] += 1

            # debug code
            # side_by_side_cmp_arr = numpy.vstack((ref_note_stops, note_stops_int, inc_mask)).T
            # print('breakpoint trap')

        note_stops = note_stops_int
        # append last note's sample counts, if necessary (i.e., if last note's duration is stored explicitly in seconds)
        if n_samples == note_stops[-1]:
            # last note has 0 samples, so remove it
            song_note_freqs = self.note_freqs[:-1]
        else:
            song_note_freqs = self.note_freqs
            note_stops = numpy.insert(note_stops, len(note_stops), n_samples)

        note_counts = note_stops[1:] - note_stops[:-1]
        note_counts = numpy.insert(note_counts, 0, note_stops[0])

        note_freqs = numpy.repeat(song_note_freqs, note_counts)
        if self.legacy_bug_null_first_sample:
            note_freqs[0] = null_note_freq

        # note_time
        # ~~~~~~~~~
        sample_indices = numpy.arange(n_samples, dtype=numpy.int64)

        if self.legacy_bug_offset:
            # we need an array of size n_samples that contains, for each sample, the offset (in eighths) of the current
            # note
            note_start_eighths = numpy.zeros(n_samples)

            # the samples in the first note have the corresponding note start index == 0, so now we address the samples
            # in the middle of the song (i.e., except the first and last notes)
            note_start_eighths[note_counts[0]: -note_counts[-1]] \
                = numpy.repeat(note_stops_eighths[:-1], note_counts[1: -1])

            # for the last note, the note start is always the last element in the note_stops_eighths array
            note_start_eighths[-note_counts[-1]:] = note_stops_eighths[-1]

            # then, we construct an array containing the offset of each sample, in eighths (the same operation we did
            # for note stops before)
            abs_offset_eighths = sample_idx2eighths(sample_indices)
            note_times_eighths = abs_offset_eighths - note_start_eighths
            note_times_seconds = note_times_eighths * self.original.period
        else:
            # first, create an array of size n_samples that contains, for each sample, the index of the first sample of the
            # corresponding note
            note_start_idx = numpy.zeros(n_samples, numpy.int64)

            # the samples in the first note have the corresponding note start index == 0, so we only need to worry about the
            # rest of the notes
            note_start_idx[note_counts[0]:] = numpy.repeat(note_stops[:-1], note_counts[1:])

            # then, construct an array containing the indices of each sample wrt the first sample of the respective note
            # using the helper array created above
            note_times = sample_indices - note_start_idx

            # optimization notice: we prefer using multiplication by the inverse rather than division by sample_rate, because
            # cpus do it faster TODO: is it worth using linspace here?
            note_times_seconds = note_times * self.period

        # apply additional note_time offset for first note (skipped samples)
        if self.first_note_time_offset_seconds:
            note_times_seconds[:note_counts[0]] += self.first_note_time_offset_seconds

        # apply note_time offset
        if self.note_time_offset_seconds:
            note_times_seconds += self.note_time_offset_seconds

        samples = voice(note_freqs, note_times_seconds)

        assert numpy.all(samples <= 1)  # to see whether we can use int16 (TODO: remove)

        samples = (samples * 32767).astype(numpy.int16)

        return samples


def sequence(score, sample_rate=44100, n_sample_offset=0, sample_seconds=None, voice=voice1,
             legacy_bug_compatibility=False, print_time=False):
    if print_time:
        import time
        start_time = time.time()

    if not sample_seconds:
        sample_seconds = score.duration_seconds

    # song_periods = song.note_stops[-1] + song.last_note_seconds * sample_rate  # float
    song_periods = score.duration_seconds * sample_rate  # float
    # some ints can't be represented as floats, so we snap to the nearest int if difference is insignificant
    # TODO: disable if messes things up
    closest_int = round(song_periods)
    if math.isclose(song_periods, closest_int):
        song_periods = int(closest_int)

    # number of samples in the song when offset is 0 (the same song can, though, have 1 fewer samples if the offset
    # causes the last note to end exactly at a sample's end)
    n_song_samples = int(song_periods)  # int (floor)

    # wrap sample start offset around end of song (even multiple times), if necessary
    n_skipped_loops = int(n_sample_offset / song_periods)
    skipped_loops_periods = n_skipped_loops * song_periods
    n_skipped_loop_samples = int(skipped_loops_periods)
    n_sample_offset -= n_skipped_loop_samples  # int

    # at every loop, note_time is accumulated (last note duration might not be divisible by sample_period)
    # note_time_offset_per_loop_seconds = math.fmod(score.duration_seconds, 1 / sample_rate)
    note_time_offset_per_loop_seconds = score.duration_seconds - n_song_samples / sample_rate
    # defend against float precision errors TODO : disable if messes things up
    if (math.isclose(note_time_offset_per_loop_seconds, 0)
            or math.isclose(note_time_offset_per_loop_seconds, 1 / sample_rate)):
        note_time_offset_per_loop_seconds = 0

    # skipped loops might introduce some note_time_offset
    note_time_offset_seconds = math.fmod(n_skipped_loops * note_time_offset_per_loop_seconds, 1 / sample_rate)

    n_full_song_loops = int(sample_seconds / score.duration_seconds)

    samples_wrap = False
    if n_full_song_loops:
        # equivalent to math.isclose(note_time_offset_per_loop_seconds, 0)
        if song_periods == n_song_samples:
            # also avoids doing fmod(..., 0) below
            n_sequences = 1
            n_repetitions = n_full_song_loops
            samples_wrap = True
        # TODO: might have to replace with math.fmod(song.period, note_time_offset_per_loop_seconds) == 0
        #  (disable if messes things up)
        elif math.isclose(math.fmod(1 / sample_rate, note_time_offset_per_loop_seconds), 0):
            # n_sequences = int(round(song.period / note_time_offset_per_loop_seconds))
            n_sequences = int(round(note_time_offset_per_loop_seconds / sample_rate))
            # n_sequences might be much higher than n_full_song_loops, case in which we don't want to turn on wrapping
            if n_full_song_loops < n_sequences:
                n_sequences = n_full_song_loops
            else:
                samples_wrap = True
            n_repetitions = int(n_full_song_loops / n_sequences)
    else:
        n_repetitions = 1

    # the number of samples that will be written to disk (note, however, that this is not, necessarily, the number of
    # samples we allocate in memory, as it's possible that on disk there will be multiple concatenated copies of what
    # we write to memory)
    n_samples = sample_seconds * sample_rate
    # preallocate output buffer (samples)
    if samples_wrap:
        samples = numpy.empty(n_sequences * score.duration_seconds * sample_rate, dtype=numpy.int16)
    else:
        samples = numpy.empty(n_samples, dtype=numpy.int16)
        # song_wraps is true if we need to patch together the last notes of the song with the first notes of the song
        song_wraps = n_sample_offset + len(samples) > n_song_samples

    song = Song.from_score(score, legacy_bug_compatibility)
    song = song.resample(sample_rate)
    song.note_time_offset_seconds = note_time_offset_seconds

    if print_time:
        voice_time = 0

    def sequencing_step(output_start_idx, crop_start_sample_idx=None, crop_stop_sample_idx=None):
        """
        helper function
        :return:
        """
        # TODO: might have to replace with note_time_offset_seconds == song.period (disable if messes things up)
        if math.isclose(song.note_time_offset_seconds, song.period):
            # song.note_time_offset_seconds = 0
            assert False, 'this should have been treated by computing n_sequences (or there might be approximation' \
                          'errors)'
        elif song.note_time_offset_seconds > song.period:
            # wrap note_time_offset_seconds around
            song.note_time_offset_seconds -= song.period

        # crop the song, if necessary
        if crop_start_sample_idx or crop_stop_sample_idx:
            seq_song = song.crop(crop_start_sample_idx=crop_start_sample_idx, crop_stop_sample_idx=crop_stop_sample_idx)
        else:
            seq_song = song

        if print_time:
            voice_start_time = time.time()

        loop_samples = seq_song.sequence(voice)

        if print_time:
            nonlocal voice_time
            voice_time += time.time() - voice_start_time

        # it's usually equal to n_song_samples, but can also be n_song_samples - 1, when at the next iteration,
        # song.note_time_offset_seconds == song.period
        n_loop_samples = len(loop_samples)

        output_stop_idx = output_start_idx + n_loop_samples
        samples[output_start_idx: output_stop_idx] = loop_samples
        song.note_time_offset_seconds += note_time_offset_per_loop_seconds

        return output_stop_idx

    output_start_idx = 0
    if not samples_wrap:
        crop_stop_sample_idx = None if song_wraps else n_sample_offset + len(samples)
        # TODO: if not song_wraps, pass crop_stop_sample_idx, too and be done with it
        output_start_idx = sequencing_step(output_start_idx, n_sample_offset, crop_stop_sample_idx)
    for _ in range(n_sequences):
        # sequence the whole song on every iteration
        output_start_idx = sequencing_step(output_start_idx)
    if not samples_wrap and song_wraps:
        # the crop_stop_sample_idx we pass makes the sanity check assertion below useless TODO: (maybe) fix
        output_start_idx = sequencing_step(output_start_idx, crop_stop_sample_idx=len(samples) - output_start_idx)

    # sanity check: output_start_idx now contains the number of samples that have been generated
    assert output_start_idx == len(samples)

    suffix_samples = None
    if samples_wrap:
        if n_sample_offset:
            # wrap (circularly) around the end of the samples
            samples = numpy.hstack((samples[n_sample_offset:], samples[:n_sample_offset]))
        suffix_size = n_samples - n_repetitions * len(samples)
        if suffix_size:
            suffix_samples = samples[:suffix_size]

    if print_time:
        end_time = time.time()
        total_time = end_time - start_time
        print('sampling time:', total_time)
        print('- voice:', voice_time)
        print('- others:', total_time - voice_time)

    return samples, n_repetitions, suffix_samples
