#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
audio sequencer module
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2021, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "bogdan@iddo.ro"
__status__ = "Development"  # can also be "Prototype" or "Production"
