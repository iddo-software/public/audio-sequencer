#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
apply a voice on song
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2021, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "bogdan@iddo.ro"
__status__ = "Development"  # can also be "Prototype" or "Production"

import math
import numpy
import progressbar


def voice1_py_legacy(note_freqs, note_times):
    from audio_sequencer.legacy_v0 import smoothstep

    assert len(note_freqs) == len(note_times)

    samples = []
    with progressbar.ProgressBar(max_value=len(note_freqs)) as bar:
        for sample_idx, (note_freq, note_time) in enumerate(zip(note_freqs, note_times)):
            left = 0.0
            right = 0.0
            i = 0
            h = float(i) / (3.0 - 1.0)

            y = (
                    0.5
                    * math.sin(6.2831 * 1.00 * note_freq * note_time)
                    * math.exp(-0.0015 * 1.0 * note_freq * note_time)
            )
            y += (
                    0.3
                    * math.sin(6.2831 * 2.01 * note_freq * note_time)
                    * math.exp(-0.0015 * 2.0 * note_freq * note_time)
            )
            y += (
                    0.2
                    * math.sin(6.2831 * 4.01 * note_freq * note_time)
                    * math.exp(-0.0015 * 4.0 * note_freq * note_time)
            )
            y += 0.1 * y * y * y
            y *= 0.9 + 0.1 * math.cos(40.0 * note_time)
            y *= smoothstep(0.0, 0.01, note_time)

            left += y * (0.5 + 0.2 * h) * (1.0 - math.sqrt(h) * 0.85)
            right += y * (0.5 - 0.2 * h) * (1.0 - math.sqrt(h) * 0.85)

            assert i == 0
            assert h == 0
            assert left == right

            samples.append(left)

            bar.update(sample_idx)


def voice1_py(note_freqs, note_times):
    assert len(note_freqs) == len(note_times)
    # preallocate output
    samples = numpy.empty(len(note_freqs))
    with progressbar.ProgressBar(max_value=len(note_freqs)) as bar:
        for sample_idx, (note_freq, note_time) in enumerate(zip(note_freqs, note_times)):
            b = note_freq * note_time
            a = math.exp(-0.0015 * b)
            a2 = a * a
            y = (0.5 * a * math.sin(6.2831 * b) +
                 0.3 * a2 * math.sin(6.2831 * 2.01 * b) +
                 0.2 * a2 * a2 * math.sin(6.2831 * 4.01 * b))

            y += 0.1 * y * y * y
            y *= 0.45 + 0.05 * math.cos(40.0 * note_time)

            x = numpy.clip(note_time * 100, 0.0, 1.0)
            y *= x * x * (3 - 2 * x)
            samples[sample_idx] = y

            bar.update(sample_idx)
    return samples


def voice1_numpy(note_freqs, note_times):
    b = note_freqs * note_times
    a = numpy.exp(-0.0015 * b)
    x = numpy.clip(note_times * 100, 0, 1)
    y = a * (0.5 * numpy.sin(6.2831 * b) +
             0.3 * a * numpy.sin(2.01 * 6.2831 * b) +
             0.2 * a * a * a * numpy.sin(4.01 * 6.2831 * b))
    y = (y + 0.1 * y * y * y) * (0.45 + 0.05 * numpy.cos(40.0 * note_times)) * x * x * (3 - 2 * x)

    return y


def voice1_numexpr(note_freqs, note_times, num_cores=None):
    import os
    if not num_cores:
        # by default, set the number of threads to the number of available cores; a user might want to spawn a different
        # number of threads, e.g., in case he wants to use some of his cores for something else
        import multiprocessing
        num_cores = multiprocessing.cpu_count()
    os.environ['NUMEXPR_NUM_THREADS'] = str(num_cores)
    import numexpr as ne

    if hasattr(ne, 'set_vml_accuracy_mode'):
        # vml accuract mode is by default 'high', but 'low' and 'fast' should work, well, faster, and they don't
        # add enough errors to change the output file (quantized to 32bits, so supports errors up to ~1e-5)
        ne.set_vml_accuracy_mode('low')

    b = ne.evaluate('note_freqs * note_times')
    a = ne.evaluate('exp(-0.0015 * b)')
    # x = numpy.clip(ne.evaluate('note_times * 100'), 0, 1)
    x = ne.evaluate('where(note_times < 0, 0, where(note_times > .01, .01, note_times)) * 100')
    y = ne.evaluate(
        'a * (0.5 * sin(6.2831 * b) + 0.3 * a * sin(2.01 * 6.2831 * b) + 0.2 * a * a * a * sin(4.01 * 6.2831 * b))'
    )
    y = ne.evaluate('(y + 0.1 * y * y * y) * (0.45 + 0.05 * cos(40.0 * note_times)) * x * x * (3 - 2 * x)')

    return y


voice1 = voice1_numexpr
