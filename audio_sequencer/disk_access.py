#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
disk access routines (file write only for now)
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2021, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "bogdan@iddo.ro"
__status__ = "Development"  # can also be "Prototype" or "Production"

import numpy


def _write_samples_to_open_wavfile(wav_file, samples, repetitions, samples_suffix, n_channels):
    # helper
    if repetitions:
        assert samples is not None
        if n_channels > 1:
            samples = numpy.repeat(samples, n_channels)
        for _ in range(repetitions):
            wav_file.writeframes(samples.tobytes())

    if samples_suffix is not None:
        if n_channels > 1:
            samples_suffix = numpy.repeat(samples_suffix, n_channels)
        wav_file.writeframes(samples_suffix.tobytes())


def _write_samples_till_stop(q, n_channels, wav_file):
    # consumer thread code
    while True:
        stuff = q.get()
        if stuff is None:
            q.task_done()
            break
        samples, repetitions, samples_suffix = stuff
        _write_samples_to_open_wavfile(wav_file, samples, repetitions, samples_suffix, n_channels)
        q.task_done()


class WavFileWriter(object):
    """
    NOTE: uses threading instead of multiprocessing, since the disk write thread is not CPU intensive (just waits for
    io), so it wouldn't benefit from running on a different core, anyway (and doesn't take CPU time from the main
    thread, either)
    """
    def __init__(self, out_filepath, n_samples, sample_rate):
        import threading
        import queue
        import wave

        # hardcoded (do we want to pass these as params?)
        self.sample_dtype = numpy.dtype(numpy.int16)
        self.n_channels = 2  # stereo

        assert numpy.issubdtype(self.sample_dtype, numpy.signedinteger)

        # since we're NOT using multiprocessing.queue, numpy arrays should not be pickled when transferred over the
        # queue (pickling of numpy arrays is quite slow). If we were to switch to multiprocessing.queue, we'd be better
        # off using shared memory instead. [Here](https://stackoverflow.com/a/38775513/2447427) is an example on how
        # that might be implemented. (note: also implement double buffering if not already in the example)
        self.q = queue.Queue()

        comptype = "NONE"
        compname = "not compressed"

        self.n_samples = n_samples

        # initialized here, because exception may be thrown while trying to build the wav_file, and, in destructor,
        # thread must be stopped before closing the file
        self.disk_thread = None

        try:
            self.wav_file = wave.open(out_filepath, "w")
            self.wav_file.setparams((self.n_channels, self.sample_dtype.itemsize, sample_rate, self.n_samples, comptype,
                                     compname))
        except:
            # mark it as None so that close does not get called in destructor
            self.wav_file = None
            raise

        # make daemonic so that it is killed when the parent is killed
        self.disk_thread = threading.Thread(target=_write_samples_till_stop,
                                            args=(self.q, self.n_channels, self.wav_file), daemon=True)
        self.disk_thread.start()

        self.n_samples_submitted = 0

    def __del__(self):
        if self.disk_thread is not None:
            # send signal to shutdown writer thread (note: we're using in-band exit event signalling because thread
            # can't detect any event while waiting to read from the queue)
            self.q.put(None)  # code for 'done writing to file'
            # wait for the queue to be empty (not really required because we wait for the writing thread to stop, too)
            self.q.join()
            # wait for write to disk to finish
            self.disk_thread.join()
            self.disk_thread = None

        # close wav file (if open succeeded)
        if self.wav_file is not None:
            self.wav_file.close()
            assert self.n_samples_submitted == self.n_samples

            # mark as closed (so that multiple calls to __del__ don't close multiple times)
            self.wav_file = None

    # object exposes context manager interface (i.e., can be used in 'with' statements)
    def __enter__(self):
        return self

    def __exit__(self, *exc):
        self.__del__()

    def _preproc(self, samples):
        if samples is not None:
            samples = numpy.asarray(samples)
            if numpy.issubdtype(samples.dtype, numpy.floating):
                assert numpy.all(samples <= 1)
                samples = (samples * numpy.iinfo(self.sample_dtype).max)
            if samples.dtype != self.sample_dtype:
                samples = samples.astype(self.sample_dtype)

            # do this inside _write_samples_till_stop so that we minimize queue traffic
            # if self.n_channels > 1:
            #     samples = numpy.repeat(samples, self.n_channels)

        return samples

    def write(self, samples, repetitions=1, suffix_samples=None):
        samples = self._preproc(samples)
        suffix_samples = self._preproc(suffix_samples)

        n_samples = 0
        if samples is not None:
            n_samples += len(samples) * repetitions
        if suffix_samples is not None:
            n_samples += len(suffix_samples)

        self.n_samples_submitted += n_samples
        if self.n_samples_submitted == self.n_samples:
            # skip threading and queue transfer and write everything at once
            _write_samples_to_open_wavfile(self.wav_file, samples, repetitions, suffix_samples, self.n_channels)
            # stop the writer thread and close the file
            self.__del__()
        else:
            self.q.put((samples, repetitions, suffix_samples))


