#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
here, we hardcode some songs we might want to sequence.
Format:
- each song is a python mapping with three (string-typed) keys: 'duration_seconds', 'seconds_per_eighth_note' and
'score'
- the value for the 'duration_seconds' key must be a real number representing the number of seconds of the melody
- the value for the 'seconds_per_eighth_note' key must be a real number representing the number of seconds assigned
to each half note
- the value for the 'notes' key must be an array of tuples. Each tuple must have two elements:
    - the first is the duration, in eighth note units, of the PREVIOUS note (alternatively, it's the offset of the
    beginning of the current note relative to the beginning of the previous note)
    - the second is the pitch of the note:
        - we use numeric codes to represent them
        - A4 has code 69
        - each code unit represents a half tone
        - as such: A has code 69, A#/B-moll has code 70, B has code 71 etc.
TODO: verify this is actually correct. The encoding has been inferred by correlating the codes with the notes
 presented in [this video](https://www.youtube.com/watch?v=Wef5lrSpiw8) I've accidentally stumbled upon, but I don't have
 a perfect pitch and was too lazy to check with a tuner. However, the intervals look right, so this might just be a
 transposition (however, this is kind of unlikely, since it's the code that assigns 440 Hz to the code 69)
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2021, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "bogdan@iddo.ro"
__status__ = "Development"  # can also be "Prototype" or "Production"


class Score(object):
    def __init__(self, duration_seconds, seconds_per_eighth_note, notes_offset_and_code):
        """
        Simple class to store melodies we can sample; also does some type checks during initialization
        :param duration_seconds: melody duration should be of numeric (real) type (can also be an int, though)
        :param seconds_per_eighth_note: melody's eighth note duration should also be of numeric (real) type (can also be
        an int, though)
        :param notes_offset_and_code: an iterable of pairs (note_relative_offset, note_code), both numeric; offsets must
        be positive and are represented in time units of length seconds_per_eighth_note
        """
        import math
        import numpy

        duration_seconds_float = float(duration_seconds)
        assert math.isclose(duration_seconds, duration_seconds_float)  # check for strings and others
        # self.duration_seconds = melody_duration_seconds_float
        self.duration_seconds = duration_seconds

        seconds_per_eighth_note_float = float(seconds_per_eighth_note)
        assert math.isclose(seconds_per_eighth_note, seconds_per_eighth_note_float)  # check for strings
        # seconds_per_eighth_note = seconds_per_eighth_note_float
        self.seconds_per_eighth_note = seconds_per_eighth_note

        # extract notes and note durations as separate arrays from the score
        notes_offset_and_code = numpy.asarray(notes_offset_and_code)
        assert notes_offset_and_code.ndim == 2
        assert notes_offset_and_code.shape[-1] == 2
        notes_rel_offset_eighths = notes_offset_and_code[:, 0]

        # melody notes and note offsets should be real numbers (also exclude complex types)
        assert numpy.issubdtype(notes_offset_and_code.dtype, numpy.integer) or numpy.issubdtype(notes_offset_and_code.dtype, numpy.floating)
        # note durations should always be (strictly) positive
        assert numpy.all(notes_rel_offset_eighths > 0)

        # TODO: find out (ask) whether:
        #  - notes and note durations are always guaranteed to be integral values
        #  - we can safely assume that 8 bits is enough to store them (i.e., that we don't use notes that are more than
        #  128 semitones below or above A4 - that's more than 10 octaves - and that no single note can have a duration
        #  longer than 256 eighths, which is 32 full notes)
        #  - if the answer to the first question is 'yes' and the answer to the second question is 'no', then we will
        #  have to generalize the code for increasing
        #  bit depths (i.e., 16 bit, 32 bit, 64 bit) by comparing the max value to the max value of each type and
        #  selecting the shortest type that can represent the data without truncation

        self.notes_offset_and_code = notes_offset_and_code


# Hedwig's theme
harry_potter_theme = Score(
    duration_seconds=40,
    seconds_per_eighth_note=0.18,
    notes_offset_and_code=[
        # the melody starts with a 10 eighth note pause
        (10, 71),  # B4  (quarter note = 2 eights)
        (2, 76),   # E5  (prolongued quarter = 3 eights)
        (3, 79),   # G5  (1 eight)
        (1, 78),   # F#5 (quarter = 2 eights)
        (2, 76),   # E5  (half note = 4 eights)
        (4, 83),   # B5  (quarter = 2 eights)
        (2, 81),   # A5  (prolongued half note = 6 eights)
        (6, 78),   # F#5 (prolongued half note = 6 eights)
        (6, 76),   # ...
        (3, 79),
        (1, 78),
        (2, 74),
        (4, 77),
        (2, 71),
        (10, 71),
        (2, 76),
        (3, 79),
        (1, 78),
        (2, 76),
        (4, 83),
        (2, 86),
        (4, 85),
        (2, 84),
        (4, 80),
        (2, 84),
        (3, 83),
        (1, 82),
        (2, 71),
        (4, 79),
        (2, 76),
        (10, 79),
        (2, 83),
        (4, 79),
        (2, 83),
        (4, 79),
        (2, 84),
        (4, 83),
        (2, 82),
        (4, 78),
        (2, 79),
        (3, 83),
        (1, 82),
        (2, 70),
        (4, 71),
        (2, 83),
        (10, 79),
        (2, 83),
        (4, 79),
        (2, 83),
        (4, 79),
        (2, 86),
        (4, 85),
        (2, 84),
        (4, 80),
        (2, 84),
        (3, 83),
        (1, 82),
        (2, 71),
        (4, 79),
        (2, 76)  # E5 - played till the end of the melody is reached
    ]
)

# test songs
test_song01 = {
    'duration_seconds': 40,  # 10 eighths
    'seconds_per_eighth_note': 0.18,
    'score': [
        # the melody starts with a 1 eighth note pause
        (1, 69),  # A  (quarter = 2 eighths)
        (2, 70),  # A# (prolongued quarter = 3 eighths)
        (3, 71),  # B  (helf note = 4 eighths, till the end)
    ]
}


def main():
    pass


if __name__ == '__main__':
    main()
