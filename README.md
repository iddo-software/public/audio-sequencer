# Audio Sequencer

This is a project written for an interview test. It showcases the skill of efficiently using
[`Python`](https://www.python.org) for signal processing tasks (specifically, resampling). The purpose of the
project is to accelerate an existing algorithm, and this implementation achieves a speed-up of about 900 times
compared to the reference implementation (200ms vs about 3 minutes). In order to achieve this level of speed-up, the
implementation was done in a  highly vectorized fashion using [`NumPy`](https://numpy.org/) and 
[`NumExpr`](https://github.com/pydata/numexpr). Also, `Python`'s built-in threading capabilities are employed for non 
CPU-intensive task parallelization.

**NOTE:** the code is NOT thoroughly tested, so it might crash for data and parameters other than the default ones

**NOTE:** Writing this code took @bbudescu about a week. 

## 1. Running the Code
1.1. Make sure that you have a reasonably recent `Python` version installed and that the default python interpreter is
   `python3` (I've used `Python` 3.8 on my Ubuntu machine).   
1.2. Create a `Python` virtual environment, activate it and install package dependencies. I've used
   [`virtualenv`](https://virtualenv.pypa.io/) from my  IDE ([PyCharm](https://www.jetbrains.com/pycharm/)), but you
   should also use be able to use the builtin [`venv`](https://docs.python.org/3/library/venv.html) module (you don't
   need to install anything  to use it, since it's packaged within recent versions of `Python`).
   [Here](https://docs.python.org/3/tutorial/venv.html) is a tutorial for `venv`. The commands you need to run should
   look something like this if you decide to use `virtualenv`:
- create a virtual environment:
```console
foo@bar:~/audio-sequencer$ virtualenv venv
```
- activate the newly created environment (from `bash`):
```console
foo@bar:~/audio-sequencer$ source venv/bin/activate
```
- install deps:
```console
(venv) foo@bar:~/audio-sequencer$ pip install -r requirements.txt
```
- **NOTE:** the last step should also install the prebuilt (`wheel`) `numexpr` package from
  [pypi](https://pypi.org/project/numexpr/). However, you might obtain better performance (most likely, this will
  happen only if you have an Intel CPU) if you
  [install `mkl`](https://software.intel.com/content/www/us/en/develop/articles/free-ipsxe-tools-and-libraries.html)
  and then
  [build and install `numexpr` yourself](https://numexpr.readthedocs.io/projects/NumExpr3/en/latest/user_guide.html#enabling-intel-vml-support),
  after  pointing it to where you installed `mkl`.
  [Here](https://numexpr.readthedocs.io/projects/NumExpr3/en/latest/intro.html#expected-performance) is the expected
  performance improvement the library's creators report. However, under limited benchmarking, I wasn't able to observe a
  drastic difference between the performance with and without `mkl` for this task, so you might as well start with the
  vanilla version of `numexpr`, from pypi.
- **NOTE:** you might be able to obtain some performance by building `numpy` yourself, as well. You can use a better
  compiler for your machine (e.g., Intel's `icc`), tweak build options (e.g., make sure `OpenMP` is enabled) and take
  into account that `numpy` supports using (optionally, if available) quite a few third party libraries to accelerate
  some of the operations it provides (e.g., `OpenBLAS`, `mkl`, `ipp` etc.).
1.3. run the code (from the working directory, with the virtual environment activated, as explained before):
```console
(venv) foo@bar:~/audio-sequencer$ python -m audio_sequencer
```
- note: you can also use PyCharm to open the project and run the `audio_sequencer` run configuration

1.4. inspect the results:
- in the working directory, you should now be able to find a file named `output.wav`. On my machine, the contents of
  that file are identical to the wav file resulted after running the code in `legacy.py`.
- in the console, you should have some prints of timing information, in seconds

## 2. Original Instructions

### 2.1. Introduction

We've got some legacy code that we'd like to run & optimize, we're  pretty much sure the code works but the guy who
coded this is not working in the company anymore. Unfortunately, we don't know how to run it anymore.

### 2.2. Instructions

- Figure out how to run the code
- Optimize the code. To do this you can use whichever strategy you might consider convenient
- Output should be exactly the same between the unoptimized & optimized version

### 2.2. Requirements

On a i7 the script is taking 175s but the previous guy told us you could run it somehow in <1s!

It's not required to optimize it to that extent though, we'd like to know what's the process you take with this type of
tasks and how you make sure you measure the performance benefits after the task has been completed.

## 3. Other Potential Future Improvements
3.1. Write Tests

3.2. High Accuracy Benchmarks

3.3. Documentation

### 3.4. GPU acceleration:
- relevant for the section that is applying the voice (i.e., obtaining samples from base note frequencies and samples'
  note times)
- I'd probably try using [`pycuda`](https://mathema.tician.de/software/pycuda/) or [`numba`](https://numba.pydata.org/),
  the latter being an optimizing compiler based on [`llvm`](https://llvm.org/) which I can see that is also able to 
  accelerate CPU code by multithreading and [SIMD](https://en.wikipedia.org/wiki/SIMD), not just generate CUDA code
- I've never used neither `pycuda`, nor numba before (although I've noticed `pycuda` to be used to implement the tensor
  computation operations in the frameworks I've used for deep learning), and would be really curious on finding out how
  they work
- another option would be to use the optimizing compilers for tensor (ndarray) operations (like
  [`Theano`](https://github.com/Theano/Theano), [`TensorFlow`](https://www.tensorflow.org/) or
  [`PyTorch`](https://pytorch.org/) which are commonly used as frameworks for building deep neural network models which
  also allow for:
    - static operation graph optimization (e.g., fusing adjacent ops) for speed and numerical stability
    - memory graph optimizations (reuse the same buffers from a preallocated pool when possible instead of allocating
      new buffers all the time)
    - generate `C/C++` and `CUDA C/C++` code by stitching together sources for known operations, compile them (this
      allows the `C`/`C++`/`CUDA` compilers to do optimization), link them in a `dll`(`pyd`)/`so`/`dylib` which exposes
      the functionality in an API that respects python extension module format, dynamically loads them via `Python`'s
      `import` mechanism, and executes the code by calling it straight from `Python`
    - these also offer high performance implementations for CPUs
    - jit compiling for performance improvements
    - if available, they can use hpc libraries (`mkl` - for intel cpus, `libm`/`acml`/`aocl` for amd cpus, `mkl-dnn`,
      `openblas`, `eigen`, `cublas`, `cudnn`, `tensorrt`, `ipp`, `opencv` etc.) for the backend
    - support several parallelization frameworks for CPU, GPU or both, like `opencl`, `halide`, `openmp`, `tbb`, `hpx`,
      `opengl`, `mpi` etc.
- yet another option would be to write the cuda code myself using the
  [`thrust` template library](https://developer.nvidia.com/thrust), as the code is not particularly  complex (thrust
  supports both cpu and gpu parallel code generation, exposes a fairly user-friendly std-like API, and does some 
  advanced optimizations for chunking arrays to fit in the gpu's shared memory)